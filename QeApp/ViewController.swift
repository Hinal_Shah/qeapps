//
//  ViewController.swift
//  QeApp
//
//  Created by Hinal Gandhi on 29/08/23.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    
    @IBOutlet weak var wkWebView: WKWebView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        loadUrl()
    }
    
    func loadUrl() {
        
        //wkWebView.navigationDelegate = self
        
        let link = URL(string: "https://www.qeapps.com/")!
        let request = URLRequest(url: link)
        wkWebView.load(request)
    }
}

